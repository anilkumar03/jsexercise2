function joinName(nameArray) {
    const Ans = [];
    for (i=0; i<nameArray.length; i++) {
        let word = nameArray[i];
        let l = word.slice(1,word.length);
        let lCase = l.toLowerCase();
        let W = word[0];
        Ans.push(W + lCase);
    }
    return Ans;
}


module.exports = joinName;