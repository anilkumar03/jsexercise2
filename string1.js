equivalentNumericFormat = (nums) =>{
    const result = [];
    for(i=0; i<nums.length; i++) {
        L = nums[i].length;
        N = "";
        for (j=0; j<L; j++) {
            if(!(nums[i][j] === "$" || nums[i][j] === ",")) {
                N += (nums[i][j]);
            }
        }
        result.push(parseFloat(N));
    }
    return result;
    
}



module.exports = equivalentNumericFormat;